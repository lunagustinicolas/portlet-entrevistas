package com.somospnt.portlet.mvc.vo;

import com.somospnt.portlet.mvc.domain.Entrevista;
import java.time.ZonedDateTime;

public class EntrevistaVo {
    private String nombre;
    private String fecha;
    private String hora;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }
    
    public Entrevista toEntrevista() {
        Entrevista entrevista = new Entrevista();
        entrevista.setNombreEntrevistado(this.getNombre());
   
        if (this.getHora() != null && this.getFecha() != null) {
            ZonedDateTime fechaPactada = ZonedDateTime.parse(this.getFecha() + "T" + this.getHora() + ":00-03:00");
            entrevista.setFechaPactada(fechaPactada);
        }

        return entrevista;
    }
    
}
