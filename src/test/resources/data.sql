INSERT INTO pais
(id,    nombre)VALUES
(1,     'Argentina'),
(2,     'Uruguay'),
(3,     'Chile');

INSERT INTO usuario(msisdn, pais_id) VALUES ('541234567890', 1);
INSERT INTO usuario(msisdn, pais_id) VALUES ('541234567891', 1);
INSERT INTO usuario(msisdn, pais_id) VALUES ('541234567892', 1);
INSERT INTO usuario(msisdn, pais_id) VALUES ('541234567893', 1);
INSERT INTO usuario(msisdn, pais_id) VALUES ('541234567894', 1);
INSERT INTO usuario(msisdn, pais_id) VALUES ('541234567895', 2);
INSERT INTO usuario(msisdn, pais_id) VALUES ('541234567896', 2);
INSERT INTO usuario(msisdn, pais_id) VALUES ('541234567897', 2);
INSERT INTO usuario(msisdn, pais_id) VALUES ('541234567898', 3);